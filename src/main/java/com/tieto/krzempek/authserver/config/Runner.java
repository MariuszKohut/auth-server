package com.tieto.krzempek.authserver.config;

import com.tieto.krzempek.authserver.entity.User;
import com.tieto.krzempek.authserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.PreparedStatement;

@Component
public class Runner implements ApplicationRunner {

	@Autowired
	private EntityManager em;

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public void run(ApplicationArguments args) throws Exception {
//		String secret = passwordEncoder.encode("secret");
//		Query query = em.createNativeQuery("INSERT INTO oauth_client_details" +
//				"(client_id, client_secret, scope, authorized_grant_types," +
//				" web_server_redirect_uri, authorities, access_token_validity," +
//				" refresh_token_validity, additional_information, autoapprove) VALUES" +
//				" (?,?,?,?,?,?,?,?,?,?)");
//		query.setParameter(1, "SampleClientId");
//		query.setParameter(2, secret);
//		query.setParameter(3, "foo,read,write");
//		query.setParameter(4, "password,authorization_code,refresh_token");
//		query.setParameter(5, null);
//		query.setParameter(6, null);
//		query.setParameter(7, 36000);
//		query.setParameter(8, 36000);
//		query.setParameter(9, null);
//		query.setParameter(10, true);
//		query.executeUpdate();
//		User user = new User();
//		user.setPassword(passwordEncoder.encode("123"));
//		user.setUsername("john");
//		userRepository.save(user);
	}
}
