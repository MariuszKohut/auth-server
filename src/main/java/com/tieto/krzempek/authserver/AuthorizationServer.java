package com.tieto.krzempek.authserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorizationServer {

	public static void main(String[] arguments) {
		SpringApplication.run(AuthorizationServer.class, arguments);
	}
}
