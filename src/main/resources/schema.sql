--create table if not exists oauth_client_details (
--    client_id VARCHAR(256) PRIMARY KEY,
--    resource_ids VARCHAR(256),
--    client_secret VARCHAR(256),
--    scope VARCHAR(256),
--    authorized_grant_types VARCHAR(256),
--    web_server_redirect_uri VARCHAR(256),
--    authorities VARCHAR(256),
--    access_token_validity INTEGER,
--    refresh_token_validity INTEGER,
--    additional_information VARCHAR(4096),
--    autoapprove VARCHAR(256)
--);

CREATE TABLE if not exists oauth_access_token (
token_id VARCHAR(255),
token LONG VARBINARY,
 authentication_id VARCHAR(255) PRIMARY KEY,
  user_name VARCHAR(255),
   client_id VARCHAR(255),
    authentication LONG VARBINARY,
     refresh_token VARCHAR(255)
     );
  CREATE TABLE if not exists oauth_refresh_token (
  token_id VARCHAR(255),
   token LONG VARBINARY,
    authentication LONG VARBINARY
    );